package com.lshershun.learning_mvi.base.di

import com.lshershun.learning_mvi.App
import com.lshershun.learning_mvi.items.details.detailsModule
import com.lshershun.learning_mvi.items.itemsModule
import com.lshershun.learning_mvi.items.list.listModule
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.fragment.koin.fragmentFactory
import org.koin.core.context.startKoin
import org.koin.core.module.Module

object Injector {
    private val modules = mutableListOf<Module>()

    fun init(app: App) {
        modules.addAll(
            listOf(
                baseModule,
                listModule,
                detailsModule,
                itemsModule,
            )
        )

        startKoin {
            androidContext(app)
            fragmentFactory()
            modules(
                modules
            )
        }
    }
}