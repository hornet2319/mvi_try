package com.lshershun.learning_mvi.base

fun <E> Iterable<E>.replace(old: E, new: E) = map { if (it == old) new else it }