package com.lshershun.learning_mvi.base.di

import com.lshershun.learning_mvi.BuildConfig
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import kotlin.time.seconds
import kotlin.time.toJavaDuration

val baseModule = module {
    single<Retrofit.Builder> {
        Retrofit.Builder()
            .client(get())
//            .addCallAdapterFactory(get())
            .addConverterFactory(get())
    }

    single<Retrofit> {
        get<Retrofit.Builder>()
            .baseUrl(BuildConfig.CONTENT_BASE_URL)
            .build()
    }

    single<OkHttpClient> {
        val builder = OkHttpClient.Builder()
        builder.addInterceptor(HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.BASIC
        })

        builder.build()
    }


    single<Moshi> {
        Moshi.Builder()
//            .add(StringDateTimeConverter())
            .addLast(KotlinJsonAdapterFactory())
            .build()
    }

    single<Converter.Factory> {
        MoshiConverterFactory.create(get())
    }
}