package com.lshershun.learning_mvi

import android.app.Application
import com.lshershun.learning_mvi.base.di.Injector

class App : Application(){

    override fun onCreate() {
        super.onCreate()
        Injector.init(this)
    }
}