package com.lshershun.learning_mvi.items.list

import com.lshershun.learning_mvi.items.data.Item

sealed class ListState {
    object Loading : ListState()
    data class Items(val items: List<Item>): ListState()
    data class Error(val message: String): ListState()
}