package com.lshershun.learning_mvi.items.details


sealed class DetailsState {
    data class EditMode(val editEnabled: Boolean) : DetailsState()
    data class DetailsReceived(val title:String, val description: String): DetailsState()
}