package com.lshershun.learning_mvi.items.data

import android.net.Uri
import android.os.Parcelable
import android.provider.OpenableColumns
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.withContext

class Repository(
    private val api: Api,
    private val dispatcher: CoroutineDispatcher,
) {

    suspend fun update(id: Int, title: String, description: String) {
//        val old = itemsList.first { it.id == id }
//        val new = old.copy(title = title, description = description)
//        itemsList = itemsList.map { if (it.id == old.id) new else it }
//
//        _items.emit(itemsList)
    }

    suspend fun getItems(): List<Item>{
        return withContext(dispatcher) {
            api.getTopAnimeList().top
        }
    }
}

fun getFilePathForN(uri: Uri): String {
    val returnCursor = context?.contentResolver?.query(uri, null, null, null, null)
    val nameIndex = returnCursor!!.getColumnIndex(OpenableColumns.DISPLAY_NAME)
    val sizeIndex = returnCursor.getColumnIndex(OpenableColumns.SIZE)
    returnCursor.moveToFirst()
    val name = returnCursor.getString(nameIndex)
    val size = returnCursor.getLong(sizeIndex).toString()
    val file = File(context?.filesDir, name)
    try {
        val inputStream = context?.contentResolver?.openInputStream(uri)
        val outputStream = FileOutputStream(file)
        val maxBufferSize = 1 * 1024 * 1024
        val bytesAvailable = inputStream!!.available()
        val bufferSize = Integer.min(bytesAvailable, maxBufferSize)
        val buffers = ByteArray(bufferSize)
        while (true) {
            val read = inputStream.read(buffers)
            if (read != -1)
                outputStream.write(buffers, 0, read)
            else break
        }
        inputStream.close()
        outputStream.close()
    } catch (e: Exception) {
        e.printStackTrace()
    }
    return file.path
}


@Parcelize
@JsonClass(generateAdapter = true)
data class Item(
    val mal_id: Int,
    val title: String,
    val start_date: String? = null,
    val end_date: String? = null,
    val score: Double? = null,
    val episodes: Int? = null
) : Parcelable {
    val description: String = "$start_date - $end_date, episodes - $episodes, rating - $score"
}

@JsonClass(generateAdapter = true)
data class TopResponse(val top: List<Item>)


