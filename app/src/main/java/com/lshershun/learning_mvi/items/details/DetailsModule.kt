package com.lshershun.learning_mvi.items.details

import org.koin.androidx.fragment.dsl.fragment
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val detailsModule = module {
    fragment {
        DetailsFragment(viewModel = get<DetailsViewModel>())
    }

    viewModel {
        DetailsViewModel(get())
    }
}
