package com.lshershun.learning_mvi.items.data

import retrofit2.http.GET

interface Api {
    @GET("top/anime/1/bypopularity")
    suspend fun getTopAnimeList(): TopResponse
}