package com.lshershun.learning_mvi.items.details

import android.os.Bundle
import android.view.*
import android.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import com.lshershun.learning_mvi.R
import com.lshershun.learning_mvi.databinding.FragmentDetailsBinding
import kotlinx.android.synthetic.main.fragment_details.*
import kotlinx.coroutines.flow.collect


class DetailsFragment(
    val viewModel: DetailsViewModel
) : Fragment(R.layout.fragment_details) {
    private var binding: FragmentDetailsBinding? = null
    private var isEditEnabled = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        lifecycleScope.launchWhenResumed {
            viewModel.state.collect {
                initState(it)
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupSimpleToolbar()
        initParams()
    }


    private fun initParams() {
        val args: DetailsFragmentArgs by navArgs()
        viewModel.setItem(args.item)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentDetailsBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }

    private fun initState(state: DetailsState) {
        when (state) {
            is DetailsState.DetailsReceived -> {
                binding?.apply {
                    title.setText(state.title)
                    description.setText(state.description)
                }
            }
            is DetailsState.EditMode -> {
                isEditEnabled = state.editEnabled

                binding?.apply {
                    if (isEditEnabled) {
                        title.post { title.requestFocus() }
                    }
                    title.isEnabled = isEditEnabled
                    description.isEnabled = isEditEnabled
                    toolbar.menu.findItem(R.id.edit).isVisible = !isEditEnabled
                    toolbar.menu.findItem(R.id.submit).isVisible = isEditEnabled
                }
            }
        }
    }

    private fun setupSimpleToolbar() {
        val navController = findNavController()
        val appBarConfiguration = AppBarConfiguration(navController.graph)
        binding?.apply {
            toolbar.setupWithNavController(navController, appBarConfiguration)
            toolbar.setOnMenuItemClickListener {
                when (it.itemId) {
                    R.id.edit -> {
                        viewModel.itemIntent.offer(DetailsIntent.EnableEditMode)
                        true
                    }
                    R.id.submit -> {
                        viewModel.itemIntent.offer(
                            DetailsIntent.EditItem(
                                title = title.text.toString(),
                                description = description.text.toString()
                            )
                        )
                        true
                    }
                    else -> {
                        false
                    }
                }
            }
        }
    }
}