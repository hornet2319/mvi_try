package com.lshershun.learning_mvi.items

import com.lshershun.learning_mvi.BuildConfig
import com.lshershun.learning_mvi.items.data.Repository
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory

import kotlinx.coroutines.Dispatchers.IO
import org.koin.dsl.module
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

val itemsModule = module {
    single {
        Repository(
            get(),
            IO
        )
    }
//    single<Retrofit.Builder> {
//        Retrofit.Builder()
//            .client(get())
//            .addCallAdapterFactory(get())
//            .addConverterFactory(get())
//    }
//
//    single<Retrofit> {
//        get<Retrofit.Builder>()
//            .baseUrl(BuildConfig.CONTENT_BASE_URL)
//            .build()
//    }
//
//
//    single<Moshi> {
//        Moshi.Builder()
//            .addLast(KotlinJsonAdapterFactory())
//            .build()
//    }
//
//    single<Converter.Factory> {
//        MoshiConverterFactory.create(get())
//    }
}