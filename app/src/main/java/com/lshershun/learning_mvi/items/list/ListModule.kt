package com.lshershun.learning_mvi.items.list

import com.lshershun.learning_mvi.items.data.Api
import com.lshershun.learning_mvi.items.data.Repository
import org.koin.androidx.fragment.dsl.fragment
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit

val listModule = module {
    fragment {
        ListFragment(viewModel = get<ListViewModel>())
    }

    viewModel {
        ListViewModel(
            repository = get<Repository>()
        )
    }

    single {
        get<Retrofit>()
            .create(Api::class.java)
    }
}
