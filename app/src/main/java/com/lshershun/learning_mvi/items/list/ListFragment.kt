package com.lshershun.learning_mvi.items.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.DividerItemDecoration.VERTICAL
import com.lshershun.learning_mvi.R
import com.lshershun.learning_mvi.databinding.FragmentListBinding
import kotlinx.coroutines.flow.collect

class ListFragment(
    val viewModel: ListViewModel
) : Fragment(R.layout.fragment_list) {
    private var binding: FragmentListBinding? = null
    private val adapter = ListAdapter {
        viewModel.itemIntent.offer(ListIntent.SelectItem(it))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lifecycleScope.launchWhenResumed {
            viewModel.navigation.collect {
                findNavController().navigate(it)
            }
        }

        lifecycleScope.launchWhenResumed {
            viewModel.state.collect {
                initState(it)
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupSimpleToolbar()

        binding?.apply {
            recycler.adapter = adapter
            recycler.addItemDecoration(DividerItemDecoration(context, VERTICAL))
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentListBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }

    private fun initState(state: ListState) {
        when (state) {
            ListState.Loading -> {
                binding?.progress?.isVisible = true
            }
            is ListState.Items -> {
                binding?.progress?.isVisible = false
                adapter.data = state.items
            }
            is ListState.Error -> {
                binding?.progress?.isVisible = false
                Toast.makeText(context, state.message, Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun setupSimpleToolbar() {
        val navController = findNavController()
        val appBarConfiguration = AppBarConfiguration(navController.graph)
        binding?.apply {
            toolbar.setupWithNavController(navController, appBarConfiguration)
        }
    }

}