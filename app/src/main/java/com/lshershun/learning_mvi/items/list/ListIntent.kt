package com.lshershun.learning_mvi.items.list

import com.lshershun.learning_mvi.items.data.Item

sealed class ListIntent {
    data class SelectItem(val item: Item): ListIntent()
}