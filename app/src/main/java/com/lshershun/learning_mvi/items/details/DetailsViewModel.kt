package com.lshershun.learning_mvi.items.details

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.lshershun.learning_mvi.items.data.Item
import com.lshershun.learning_mvi.items.data.Repository
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.consumeAsFlow
import kotlinx.coroutines.launch

class DetailsViewModel(private val repository: Repository) : ViewModel() {
    val itemIntent = Channel<DetailsIntent>(Channel.UNLIMITED)
    private val _state: MutableStateFlow<DetailsState> =
        MutableStateFlow(DetailsState.EditMode(false))
    val state: StateFlow<DetailsState> = _state

    init {
        viewModelScope.launch {
            itemIntent.consumeAsFlow().collect {
                handleIntent(it)
            }
        }
    }

    private var id: Int = 0

    fun setItem(item: Item) {
        id = item.mal_id
        viewModelScope.launch {
            _state.emit(DetailsState.DetailsReceived(item.title, item.description))
        }
    }

    private fun handleIntent(intent: DetailsIntent) {
        when (intent) {
            is DetailsIntent.EditItem -> {
                _state.value = DetailsState.EditMode(false)
                viewModelScope.launch {
                    repository.update(id, intent.title, intent.description)
                }
            }
            DetailsIntent.EnableEditMode -> {
                _state.value = DetailsState.EditMode(true)
            }
        }
    }
}