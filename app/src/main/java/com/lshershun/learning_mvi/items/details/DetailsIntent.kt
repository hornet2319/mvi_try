package com.lshershun.learning_mvi.items.details


sealed class DetailsIntent {
    data class EditItem(val title:String, val description:String): DetailsIntent()
    object EnableEditMode: DetailsIntent()
}