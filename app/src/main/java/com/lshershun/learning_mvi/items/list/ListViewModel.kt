package com.lshershun.learning_mvi.items.list

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.navigation.NavDirections
import com.lshershun.learning_mvi.items.data.Item
import com.lshershun.learning_mvi.items.data.Repository
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch

class ListViewModel(private val repository: Repository) : ViewModel() {
    val itemIntent = Channel<ListIntent>(Channel.UNLIMITED)
    private val _state: MutableStateFlow<ListState> = MutableStateFlow(ListState.Loading)
    val state: StateFlow<ListState> = _state

    private val _navigation = MutableSharedFlow<NavDirections>()
    val navigation: Flow<NavDirections> = _navigation

    init {
        viewModelScope.launch {
            itemIntent.consumeAsFlow().collect {
                handleIntent(it)
            }
        }
        viewModelScope.launch {
            try {
                _state.value = ListState.Items(repository.getItems())
            } catch (t: Throwable) {
                Log.e("tag",  " fetch list ", t)
            }
        }
    }

    private fun handleIntent(intent: ListIntent) {
        when (intent) {
            is ListIntent.SelectItem -> {
                showItemDetails(intent.item)
            }
        }
    }

    private fun showItemDetails(item: Item) {
        viewModelScope.launch {
            _navigation.emit(ListFragmentDirections.actionListToDetails(item))
        }
    }
}