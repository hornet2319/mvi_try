package com.lshershun.learning_mvi.items.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.lshershun.learning_mvi.R
import com.lshershun.learning_mvi.databinding.ListItemBinding
import com.lshershun.learning_mvi.items.data.Item

class ListAdapter(
    private val onItemClick: (Item) -> Unit
): RecyclerView.Adapter<ListAdapter.ViewHolder>(){

    var data: List<Item> = emptyList()
    set(value) {
        field = value
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.list_item, parent, false)
        return ViewHolder(view) {
            onItemClick(data[it])
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(data[position])
    }

    override fun getItemCount() = data.size


    class ViewHolder(
        view:View,
        onItemClicked: (Int) -> Unit
    ): RecyclerView.ViewHolder(view) {
        private val binding = ListItemBinding.bind(view)

        init {
            binding.root.setOnClickListener {
                onItemClicked(adapterPosition)
            }
        }

        fun bind(item: Item) {
            binding.description.text = item.description
            binding.title.text = item.title
        }
    }
}