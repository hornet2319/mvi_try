package com.lshershun.learning_mvi

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.test.*
import org.junit.After
import org.junit.Before

abstract class CoroutineTestBase {
    protected val testDispatcher = TestCoroutineDispatcher()
    protected val testScope = TestCoroutineScope(testDispatcher)

    @Before
    open fun setMainDispatcher() {
        Dispatchers.setMain(testDispatcher)
    }

    @After
    open fun cleanupTestCoroutines() {
        Dispatchers.resetMain()
        testScope.cleanupTestCoroutines()
    }

    open fun runBlockingTest(block: suspend TestCoroutineScope.() -> Unit) {
        testScope.runBlockingTest { block() }
    }

    fun <T> Flow<T>.test(): TestFlow<T> {
        val testFlow = TestFlow(this)
        testFlow.launchIn(testScope)
        return testFlow
    }
}