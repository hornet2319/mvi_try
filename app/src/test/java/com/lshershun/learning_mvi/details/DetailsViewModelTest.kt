package com.lshershun.learning_mvi.details


import com.lshershun.learning_mvi.CoroutineTestBase
import com.lshershun.learning_mvi.items.Item
import com.lshershun.learning_mvi.items.Repository
import com.lshershun.learning_mvi.items.details.DetailsIntent
import com.lshershun.learning_mvi.items.details.DetailsState
import com.lshershun.learning_mvi.items.details.DetailsViewModel
import io.mockk.coVerify
import io.mockk.mockk

import org.junit.Assert
import org.junit.Test

class DetailsViewModelTest : CoroutineTestBase() {
    private val detailsRepository = mockk<Repository>(relaxed = true)

    private val viewModel by lazy { DetailsViewModel(detailsRepository) }

    @Test
    fun `item selection should trigger correct state`() = runBlockingTest {
        viewModel.setItem(TEST_ITEM)
        val lastState = viewModel.state.test().values.last()

        Assert.assertTrue(lastState is DetailsState.DetailsReceived)
    }

    @Test
    fun `update item intent should trigger corresponding action on repository`() = runBlockingTest {
        viewModel.itemIntent.offer(DetailsIntent.EditItem(TEST_ITEM.title, TEST_ITEM.description))
        coVerify {
            detailsRepository.update(any(), TEST_ITEM.title, TEST_ITEM.description)
        }
    }

    @Test
    fun `turn off edit mode should change status correctly`() = runBlockingTest {
        viewModel.itemIntent.offer(DetailsIntent.EditItem(TEST_ITEM.title, TEST_ITEM.description))

        val lastState = viewModel.state.test().values.last()
        val editEnabled = lastState
            .let { it as DetailsState.EditMode }
            .editEnabled

        Assert.assertTrue(lastState is DetailsState.EditMode)
        Assert.assertFalse(editEnabled)
    }

    @Test
    fun `turn on edit mode should change status correctly`() = runBlockingTest {
        viewModel.itemIntent.offer(DetailsIntent.EnableEditMode)
        val lastState = viewModel.state.test().values.last()
        val editEnabled = lastState
            .let { it as DetailsState.EditMode }
            .editEnabled

        Assert.assertTrue(lastState is DetailsState.EditMode)
        Assert.assertTrue(editEnabled)
    }

}

val TEST_ITEM = Item(0, "test", "test")